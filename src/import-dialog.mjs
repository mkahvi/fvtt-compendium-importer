import { CFG } from './common.mjs';
import { createCompendium } from './create-compendium.mjs';

class DocumentWrapper {
	document;
	json;
	documentName;
	constructor(data, documentName) {
		this.documentName = documentName;
		const docClass = CONFIG[documentName].documentClass;
		this.document = new docClass.implementation(data);
		this.json = data;
	}

	get id() {
		return this.document.id;
	}

	get name() {
		return this.json.name;
	}

	get img() {
		return this.json.img;
	}

	get type() {
		return this.json.type;
	}

	get folder() {
		return this.json.folder;
	}
}

export class ImportDialog extends FormApplication {
	data = null;

	pack = null;

	conflict = false;

	importOptions = {
		keepId: true,
	};

	wrongSystem = false;

	wrongSystemIgnore = false;

	items = new Collection();
	folders = new Collection();

	tree = null;

	constructor(json, pack, options) {
		super(undefined, options);
		this.data = json;
		this.pack = pack;

		for (const item of json.items ?? [])
			this.items.set(item.id, item);

		for (const folder of json.folders ?? [])
			this.folders.set(folder._id, { ...folder, json: folder, get id() { return this._id; }, items: [], folders: [] });

		this.updateSlug();
		this.wrongSystem = this.data.source.system !== game.system.id;

		this.tree = this.buildTree();
	}

	updateSlug() {
		this.pack.slug = this.pack.label.slugify();
		const packName = `world.${this.pack.slug}`;
		const existing = game.packs.get(packName);
		if (existing) {
			console.warn('Pack name causes conflict with existing pack:', packName);
		}
		this.conflict = !!existing;
		return this.conflict;
	}

	get template() {
		return `modules/${CFG.id}/template/import-dialog.hbs`;
	}

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			title: game.i18n.localize('CompendiumImporter.Import'),
			// resizable: true,
			classes: [...options.classes, 'mkah-compendium-importer'],
			dragDrop: [{ dragSelector: '.tree .items .document' }],
			closeOnSubmit: false,
			submitOnChange: true,
			submitOnClose: false,
			height: 'auto',
			width: 540,
		};
	}

	buildTree() {
		const tree = { items: [], folders: [], lost: [] };

		tree.folders = this.folders.filter(f => !f.folder);

		// Assign items to folders
		for (const item of this.items) {
			const parent = item.folder;
			let folder;
			if (parent && (folder = this.folders.get(parent))) {
				folder.items.push(item);
			}
			else {
				tree.items.push(item);
			}
		}

		const deepFolders = this.folders.filter(f => !!f.folder);

		// Assign folders to folders
		for (const folder of deepFolders) {
			const parent = folder.folder;
			let pfolder;
			if (parent && (pfolder = this.folders.get(parent))) {
				pfolder.folders.push(folder);
			}
			else {
				folder.lost = true;
				tree.lost.push(folder);
			}
		}

		return tree;
	}

	getData() {
		const data = super.getData();
		data.metadata = this.data?.metadata;
		data.data = this.data;
		data.ready = true;
		data.pack = this.pack;

		data.items = this.items;
		data.folder = this.data?.folder;
		data.folders = this.folders;

		data.tree = this.tree;

		data.keepId = this.importOptions.keepId;
		data.nameConflict = this.conflict;
		data.wrongSystem = this.wrongSystem;
		data.ignoreSystem = this.wrongSystemIgnore;
		data.disableSubmit = this.conflict || this.wrongSystem && !this.wrongSystemIgnore;
		return data;
	}

	_canDragStart(selector) {
		return true;
	}

	/**
	 * @param {DragEvent} event
	 */
	_onDragStart(event) {
		const el = event.currentTarget;
		const { documentId, documentKind, documentType } = el.dataset;
		const doc = this.data.items.find(i => i.id === documentId);

		const dragData = { data: deepClone(doc.json), type: documentKind };
		if (game.release.generation < 11) {
			// In v10 dropping to sidebar simply does not work if the ID is left in place
			delete dragData.data._id;
		}
		event.dataTransfer.setData('text/plain', JSON.stringify(dragData));
	}

	_updateObject(event, data) {
		this.pack.label = data.label;
		this.importOptions.keepId = data.keepId;
		this.wrongSystemIgnore = data.ignoreSystem;
		this.updateSlug();
		this.render();
	}

	activateListeners(html) {
		super.activateListeners(html);

		html[0].querySelector('button.submit').addEventListener('click', this._createCompendium.bind(this));
	}

	async _createCompendium(ev) {
		ev.preventDefault();
		ev.stopPropagation();

		ev.target.disabled = true;

		createCompendium(this.pack, this.data, this.importOptions);

		this.close();
	}

	static async fromJSON(data, options) {
		let valid = true, json = null;

		try {
			json = JSON.parse(data);
		}
		catch (err) {
			ui.notifications.error(err.message, { console: false });
			console.error(err);
			valid = false;
			return;
		}

		// Basic validation
		if (!Array.isArray(json.items)) valid = false;
		if (!json.metadata) valid = false;

		const docClass = CONFIG[json.type].documentClass;
		if (!docClass) {
			ui.notifications.error(`${json.type} is invalid document class`);
			valid = false;
		}

		if (!valid) return void ui.notifications.error(game.i18n.localize('CompendiumImporter.BadDataImport'));

		json.items = json.items.map(docData => {
			try {
				return new DocumentWrapper(docData, json.type);
			}
			catch (err) {
				console.error(err);
			}
			return null;
		}).filter(i => !!i);

		const pack = {
			label: json.metadata.label,
			id: json.metadata.name,
			type: json.metadata.entity ?? json.metadata.type,
		};

		new ImportDialog(json, pack, options).render(true, { focus: true });
	}
}
