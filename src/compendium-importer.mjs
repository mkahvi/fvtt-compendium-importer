import { CFG } from './common.mjs';
import { ImportDialog } from './import-dialog.mjs';
import './settings.mjs';

function exportDocument(doc) {
	const obj = doc.toCompendium(); // .toCompendium() is used for exporting by core Foundry
	obj._id = doc.id; // preserve ID
	return obj;
}

async function exportCompendium(li) {
	const packId = li.get(0).dataset.pack;
	const pack = game.packs.get(packId);

	if (!pack)
		return void ui.notifications.warn(game.i18n.format('CompendiumImporter.PackNotFound', { pack: packId }));

	ui.notifications.info(game.i18n.format('CompendiumImporter.StartExport', { pack: packId, count: pack.index.contents.length }));

	const data = {
		package: pack.collection, // `${metadata.package}.${metadata.name}`
		metadata: duplicate(pack.metadata),
		type: pack.documentName,
		items: [],
		source: {
			world: game.world.id,
			system: game.system.id,
			version: {
				core: game.version,
				system: game.system.version,
			},
		},
	};

	const items = await pack.getDocuments();
	data.items.push(...items.map(exportDocument));

	if (game.release.generation >= 11) {
		data.folders = pack.folders.map(exportDocument);
		if (pack.folder) {
			data.folder = { id: pack.folder.id, name: pack.folder.name };
		}
	}

	const filename = `fvtt-${data.type}-pack-${game.system.id}-${pack.metadata.name}.json`;

	ui.notifications.info(game.i18n.format('CompendiumImporter.FinishExport', { pack: packId }));

	/* global saveDataToFile */
	saveDataToFile(JSON.stringify(data, null, 2), 'text/json', filename);
}

/**
 * Foundry v12 and newer
 *
 * @param {DocumentDirectory} html
 * @param app
 * @param {object[]} entries
 */
function insertExportMenuItem(app, entries) {
	const exportEntry = {
		name: 'CompendiumImporter.Export',
		icon: '<i class="fas fa-file-export"></i>',
		callback: exportCompendium,
	};
	entries.push(exportEntry);
}

/**
 * Foundry v11 and older
 *
 * @param {JQuery} html
 * @param {object[]} entries
 */
function insertExportMenuItemV11([html], entries) {
	const exportEntry = {
		name: 'CompendiumImporter.Export',
		icon: '<i class="fas fa-file-export"></i>',
		callback: exportCompendium,
	};
	entries.push(exportEntry);
}

/**
 * @param {CompendiumDirectory} cc
 * @param {JQuery} html
 * @param {object} opts
 */
function insertImportButton(cc, [html], opts) {
	if (!game.user.isGM) return;

	const buttons = html.querySelector('header .action-buttons');
	const btn = document.createElement('button');
	if (game.release.generation >= 11) btn.classList.add('ic-wide');
	btn.classList.add('import-compendium');
	btn.type = 'button';
	btn.innerHTML = `<i class="fa-solid fa-upload"></i> ${game.i18n.localize('CompendiumImporter.ImportShort')}`;
	buttons.append(btn);
	btn.addEventListener('click', (ev) => {
		ev.preventDefault();
		ev.stopPropagation();

		const options = { left: window.innerWidth - 620, top: this.offsetTop + 20 };

		const fileRead = document.createElement('input');
		fileRead.type = 'file';
		fileRead.accept = 'application/json';
		fileRead.enctype = 'multipart/form-data';

		fileRead.addEventListener('change', function readFile(evt) {
			const files = evt.target.files,
				reader = new FileReader();

			reader.onload = (event) => {
				options.left = window.innerWidth - 860;
				ImportDialog.fromJSON(event.target?.result, options);
			};

			for (const file of files)
				reader.readAsText(file);
		}, { passive: true, once: true });

		fileRead.click();
	});
}

Hooks.once('init', () => {
	if (game.release.generation >= 12)
		Hooks.on('getCompendiumDirectoryEntryContext', insertExportMenuItem);
	else
		Hooks.on('getCompendiumDirectoryEntryContext', insertExportMenuItemV11);
});

Hooks.on('renderCompendiumDirectory', insertImportButton);
