import { CFG } from './common.mjs';

/**
 * @param {CompendiumCollection} pack
 * @param {object} json
 * @param {object} options
 * @param {boolean} options.keepId
 */
async function fillCompendium(pack, json, { keepId = true } = {}) {
	const options = {
		pack: pack.collection,
		keepId,
	};

	const creationData = json.items.map(i => {
		const o = i.document.toObject();
		o.folder = i.json.folder;
		if (!o.folder) delete o.folder;
		if (!o._id) delete o._id; // Remove invalid ids, just in case
		return o;
	});

	if (creationData.length) {
		let chunkSize = game.settings.get(CFG.id, 'chunking');
		if (chunkSize <= 0) chunkSize = Infinity;

		// Split data into chunks
		const chunks = [[]];
		for (const entry of creationData) {
			const last = chunks.at(-1);
			if (last.length < chunkSize)
				last.push(entry);
			else
				chunks.push([entry]);
		}

		let i = 1;
		for (const chunk of chunks) {
			console.log('Creating chunk', i++, 'of', chunks.length);
			await pack.documentClass.create(chunk, foundry.utils.deepClone(options));
		}
	}

	ui.notifications.info(game.i18n.format('CompendiumImporter.FinishImport', { pack: pack.collection }));
}

/**
 * @param {CompendiumCollection} pack
 * @param {object} json
 * @param {object} options
 * @param {boolean} options.keepId
 */
async function setupFolders(pack, json, { keepId = true } = {}) {
	if (game.release.generation < 11) return;

	const folders = json.folders ?? [];
	if (folders.length == 0) return;

	return Folder.create(json.folders, { pack: pack.collection, keepId: true });
}

export async function createCompendium(packData, json, { keepId = true } = {}) {
	const createData = {
		label: packData.label,
		type: json.type,
	};

	const pack = await CompendiumCollection.createCompendium(createData);

	if (json.folder) {
		if (game.folders.get(json.folder))
			await pack.setFolder(json.folder);
		else
			console.warn(`Folder "${json.folder}" not found.`);
	}

	const folders = await setupFolders(pack, json, { keepId });

	await fillCompendium(pack, json, { keepId });
}
