import { CFG } from './common.mjs';

Hooks.once('init', () => {
	game.settings.register(CFG.id, 'chunking', {
		name: 'CompendiumImporter.Settings.Chunking',
		hint: 'CompendiumImporter.Settings.ChunkingHint',
		type: Number,
		default: 0,
		range: {
			min: 0,
			step: 100,
			max: 1000,
		},
		config: true,
		scope: 'client'
	});
});
