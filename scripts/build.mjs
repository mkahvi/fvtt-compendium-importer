import path from 'node:path';
import esbuild from 'esbuild';
import process from 'node:process';
import fs from 'node:fs';

const OUT_DIR = './dist';

const packData = fs.readFileSync('./package.json');
const packJSON = JSON.parse(packData);
const mainFile = path.join('src', packJSON.source);

const args = process.argv.slice(2);
const watch = args.includes('--watch');

async function build() {
	const ctx = await esbuild.context({
		entryPoints: [mainFile],
		target: ['es2022'],
		bundle: true,
		// absWorkingDir: path.resolve(OUT_DIR),
		// outfile: path.join(OUT_DIR, path.parse(mainFile).base),
		outdir: path.resolve(OUT_DIR),
		format: 'esm',
		outExtension: { '.js': '.mjs' },
		metafile: true,
		sourcemap: true,
		minify: !watch,
		minifyIdentifiers: false,
		minifyWhitespace: !watch,
		minifySyntax: !watch,
		keepNames: true,
		platform: 'browser',
		logLevel: 'info',
		logLimit: 0,
		treeShaking: true,
		color: true,
		external: [
			// ...externalizedEsm.map(f => path.resolve(f)),
			// '/tests/*',
			'/node_modules/*'
		],
	});

	let res;
	try {
		if (watch) res = await ctx.watch();
		else res = await ctx.rebuild();
	}
	catch (err) {
		console.error(err);
	}

	// Display size of sources
	if (res) {
		try {
			const originalSizeB = Object.values(res.metafile.inputs).reduce((t, i) => t + i.bytes, 0);
			const files = Object.entries(res.metafile.inputs).reduce((t, [file, data]) => {
				t.add(file);
				data.imports.forEach(d => t.add(d.path));
				return t;
			}, new Set());

			let outSize = 0, mapSize = 0;
			Object.entries(res.metafile.outputs).forEach(([file, data]) => {
				if (/\.map/.test(file))
					mapSize += data.bytes;
				else
					outSize += data.bytes;
			});

			const kB = (b) => Math.round(b / 100) / 10;

			const savings = originalSizeB - outSize,
				percentage = Math.round(((outSize / originalSizeB) * 1_000)) / 10;
			console.log('- Original:', kB(originalSizeB), 'kB in', files.size, 'files\n- Final:', kB(outSize), 'kB (Map:', kB(mapSize), 'kB)\n- Savings:', kB(savings), 'kB —', percentage, '%\n');
		}
		catch (err) {
			console.error(err);
		}
	}

	if (!watch) ctx.dispose();
}

await build();
