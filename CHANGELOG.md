# Change Log

## 2.3.0

- Foundry v12 compatibility.

## 2.2

- New: Compendium creation chunking option.
  Creates compendium content in chunks. Defaults to disabled.

## 2.1.0

- New: Added support for compendium folders in Foundry v11.
- New: You can drag&drop documents directly from import dialog without needing to import everything.
- Fix: Import button styling in Foundry v11

## 2.0.1

- Import button skips the intermediate dialog and opens file select dialog directly.
- File select dialog now specifically requests for `.json` (via `application/json`).

## 2.0.0

- Foundry v10 compatibility
- Support for v9 and older dropped
- New release mechanism for smaller download and install sizes

## 1.1.1

- Internal: Bundling via esbuild.
- Internal: Less swapped for SCSS.

## 1.1

- Future proofing.

## 1.0.4.1

- Fixed: Minor Foundry v9 compatibility issue.

## 1.0.4

- New: Improved translation support.
- Changed: Ignore system toggle is disabled instead of hidden for same system imports.

## 1.0.1 – 1.0.3 Various fixes

## 1.0.0 Initial
